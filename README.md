# Arne Van Den Kerchove

[Professional Resume](https://gitlab.com/arnevdk/curriculum-vitae/-/jobs/artifacts/master/raw/out/professional_cv.pdf?job=compile)

[Academic CV](https://gitlab.com/arnevdk/curriculum-vitae/-/jobs/artifacts/master/raw/out/academic_cv.pdf?job=compile)

https://arne.vandenkerchove.com
